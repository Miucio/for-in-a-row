//
// Created by Federico Cosimo Lapenna on 18/05/2020.
//

#include <iostream>
#include <cstring>
#include "../src/server/ServerOp.h"
#include "../src/common/operations/Operations.h"
#include "../src/common/typeMessage.h"
#include "../src/client/ClientOp.h"

using namespace std;

int testCryptDecript();
int testSendEncryptCertificate();
int TestListUser();

int main(int argc, char *argv[]) {
    //testCryptDecript();
    //testSendEncryptCertificate();
    TestListUser();
    return 0;

}

int testCryptDecript() {
    vector<unsigned char> session_key = GenerateRandomSequece(SESSIONKEYSIZE);
    vector<unsigned char> ct;
    Message msg;

    cout << "chiave: " << session_key.data() << '\n';

    char *message = new char[35];
    memcpy(message, "La sicurezza é bella e tenera", strlen("La sicurezza é bella e tenera"));
    size_t t = strlen("La sicurezza é bella e tenera");

    cout << "Il messaggio di test é: " << message << '\n';
    ct = EncryptMessage(message, t, session_key);
    if (ct.empty()) {
        cerr << "testo criptato nullo" << '\n';
    }

    msg.setCt(ct, ct.size());
    Message finalMessage = DecryptMessage(msg, session_key);

    cout << "Il messaggio di test dec é: " << finalMessage.pt.data() << '\n' << '\n';

}

//int testSendEncryptCertificate(){
//    Server *server = new Server();
//    server->Init(9999);
//    vector<unsigned char>sk;
//    sk.insert(sk.begin(), "fjdhfgjhs67842nkjsfhg783t2", "fjdhfgjhs67842nkjsfhg783t2" + strlen("fjdhfgjhs67842nkjsfhg783t2"));
//    SendEncryAndSignCertificate(server->certificate,2,sk,server->pkey);
//
//
//}

int TestListUser(){
    vector<unsigned char> users;

    users.insert(users.begin(),"pippo/jjdiwj/fbjsdh/fhusdhg/fhdsuh","pippo/jjdiwj/fbjsdh/fhusdhg/fhdsuh" + strlen("pippo/jjdiwj/fbjsdh/fhusdhg/fhdsuh"));
    //list<unsigned char*>elements = GetListByMessage(users);
}

