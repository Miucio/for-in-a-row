//
// Created by Federico Cosimo Lapenna on 29/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_TYPEMESSAGE_H
#define CLIENTPROJECTSECURITY_TYPEMESSAGE_H

#define SESSIONKEYSIZE 32
#define TAGSIZE 16
#define USER_NAME_SIZE_MAX 20
#define SIGNATURELEN 256
#define IV_DIM_GCM 12

#define FRIEND_NONCE 1
#define MY_NONCE 2

#define CHALLENGEPORT 6060

#define GCM_MESSAGE 1
#define NO_CRIPT_MESSAGE 0
#define CHECK_SIGN 1
#define NO_CHECK_SIGN 0
#define CLOSECONN -2

#define SUCCEED  1
#define FAIL -1

#define INT_ATTRIBUTE_DIM 4
#define RESPONSE_ATTRIBUTE_DIM 100
#define ADDRESS_ATTRIBUTE_DIM 15

#define CLIENT_HELLO_TYPE 1
#define SERVER_HELLO_TYPE 2
#define CLIENT_VERIFY_TYPE 3
#define OK_TYPE 4
#define REJECT_TYPE 5
#define ERROR_TYPE 6
#define AVAILABLE_USERS_TYPE 7
#define CHALLENGE_USER_TYPE 8
#define CHALLENGES_RECEIVED_TYPE 9
#define ACCEPT_TYPE 10
#define FREE_CLIENT_TYPE 11
#define OPPONENT_ADDRESS_TYPE 12
#define CLOSE_CONN_TYPE 13
#define GAME_TYPE 14
#define SHARE_KEY_TYPE 15

static const char *TYPE_ATTRIBUTE = "TypeMessage";
static const char *CERTIFICATE_ATTRIBUTE = "certificate";
static const char *CERTIFICATE_LENGTH_ATTRIBUTE = "certificatelength";
static const char *NONCE_ATTRIBUTE = "Nonce";
static const char *RESPONSE_NONCE_ATTRIBUTE = "ResponseNonce";
static const char *USER_NAME_ATTRIBUTE = "UserName";
static const char *SIGNATURE_ATTRIBUTE = "Signature";
static const char *SIGN_LEN_ATTRIBUTE = "Sign_len";
static const char *RESPONSE_ATTRIBUTE = "Response";
static const char *MESSAGE_ATTRIBUTE = "Payload";
static const char *USERS_ATTRIBUTE = "users";
static const char *PUB_KEY_ATTRIBUTE = "pubKey";
static const char *ADDRESS_ATTRIBUTE = "address";
static const char *PORT_ATTRIBUTE = "port";
static const char *COLUMN_ATTRIBUTE = "column";

#define MESSAGE_FORMAT "<Body><Signature>%s<Signature><Sign_len>%d<Sign_len><Payload>%s<Payload><Body>"

#define CLIENT_HELLO_TYPE_DIM 12
#define SERVER_HELLO_TYPE_DIM 12
#define CLIENT_VERIFY_TYPE_DIM 13
#define OK_TYPE_DIM 10
#define REJECT_TYPE_DIM 14
#define ERROR_TYPE_DIM 13
#define AVAILABLE_USERS_TYPE_DIM 14
#define CHALLENGE_USER_TYPE_DIM 14
#define CHALLENGES_RECEIVED_TYPE_DIM 19
#define ACCEPT_TYPE_DIM 16
#define FREE_CLIENT_TYPE_DIM 11
#define OPPONENT_ADDRESS_TYPE_DIM 18
#define CLOSE_CONN_TYPE_DIM 10
#define GAME_TYPE_DIM 4
#define SHARE_KEY_TYPE_DIM 9


#endif //CLIENTPROJECTSECURITY_TYPEMESSAGE_H
