//
// Created by Federico Cosimo Lapenna on 09/05/2020.
//

#include <iostream>
#include <openssl/ossl_typ.h>
#include <openssl/rand.h>
#include <sys/socket.h>
#include <cstring>
#include <utility>
#include <vector>
#include <openssl/pem.h>
#include "Operations.h"
#include "KeyExchange.h"

using namespace std;

#define BUFFER 2048

unsigned int IV_nonce = 1;

int SessionKeyNegotation(Session *pSession, EVP_PKEY *pkey) {
    Payload payload;

    vector<unsigned char> skey = getDHCommonKey(pSession, pkey);
    if (skey.empty()) {
        cerr << "dh key is null" << '\n';
        return FAIL;
    }

    vector<unsigned char> my_session_key = GenerateRandomSequece(SESSIONKEYSIZE);
    if (my_session_key.empty()) {
        cerr << "session key is null" << '\n';
    }

    payload.setType(SHARE_KEY_TYPE, SEND_MSG);
    if (payload.setKey(my_session_key.data(), my_session_key.size(), SEND_MSG) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    vector<unsigned char> send_message = payload.getPayload();
    send_message = EncryptMessage(send_message.data(), send_message.size(), skey, pSession);
    if (send_message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, send_message, send_message.size(), pkey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        return FAIL;
    }

    Message rec_message = ReceiveMessage(pSession, GCM_MESSAGE, NO_CHECK_SIGN);
    if (rec_message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }
    rec_message = DecryptMessage(rec_message, skey, pSession);
    if (rec_message.pt.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }

    Payload recv_payload;
    if (recv_payload.setPeyload(rec_message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (recv_payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(recv_payload.type, SHARE_KEY_TYPE) < 0) {
        char *err = "type massage is wrong";
        cerr << err << '\n';
        if (SendError(pSession, pkey, err, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
            cerr << "Error to send error message to server " << "\n";
            return FAIL;
        }
    }

    vector<unsigned char> friend_session_key = recv_payload.key;
    if (friend_session_key.empty()) {
        cerr << "friend dh key is empty" << "\n";
        return FAIL;
    }

    vector<unsigned char> final_session_key;

    //XOR BIT A BIT
    final_session_key.reserve(SESSIONKEYSIZE);
    for (int i = 0; i < SESSIONKEYSIZE; i++) {
        final_session_key.push_back(friend_session_key.at(i) ^ my_session_key.at(i));
    }

    pSession->setSessionKey(final_session_key, final_session_key.size());

    if (SendOk(pSession, pkey, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        char *err = "Error when try to send ok after autentication";
        cerr << err << '\n';
        if (SendError(pSession, pkey, err, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
            cerr << "Error to send error message to server " << "\n";
            pSession->cleanSessionKey();
            return FAIL;
        }
        return FAIL;

    }
    if (WaitResponse(pSession, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "session key is not set corretly" << '\n';
        pSession->cleanSessionKey();
        return FAIL;
    }
    cout << "session key ricevuta!" << '\n';

    return SUCCEED;
}

//deserialize pubkey
vector<unsigned char> getPubKey(EVP_PKEY *key) {
    vector<unsigned char> v;

    BIO *publicBIO = BIO_new(BIO_s_mem());
    if (!publicBIO) {
        cerr << "Bio is empty" << '\n';
        return v;
    }
    // dump key to IO
    PEM_write_bio_PUBKEY(publicBIO, key);
    // get buffer length
    int publicKeyLen = BIO_pending(publicBIO);
    if (publicKeyLen <= 0) {
        cerr << "public key len invalid" << '\n';
        return v;
    }
    // create char reference of public key length
    auto *publicKeyChar = new unsigned char[publicKeyLen];
    // read the key from the buffer and put it in the char reference
    BIO_read(publicBIO, publicKeyChar, publicKeyLen);
    // at this point we can save the public somewhere

    v.insert(v.begin(), publicKeyChar, publicKeyChar + publicKeyLen);

    return v;
}

int SendCertificate(Session *pSession, X509 *pCertificate, EVP_PKEY *pKey) {
    int bytes;
    unsigned char *pCert = nullptr;
    vector<unsigned char> cert;
    vector<unsigned char> send_message;

    bytes = i2d_X509(pCertificate, &pCert); //SERIALIZE CERTIFICATE
    if (bytes <= 0) {
        cerr << "Error:To serialize the certificate, impossible complete the connection " << "\n";
        goto err;
    }
    cert.insert(cert.begin(), pCert, pCert + bytes);

    if (SendMessage(pSession->session_id, cert, cert.size(), pKey, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send hello message" << "\n";
        goto err;
    }

    return SUCCEED;

    err:
    return FAIL;
}

Message DecryptByGCM(Message message, vector<unsigned char> key) {
    EVP_CIPHER_CTX *ctx;
    if (message.ct_t == 0) {
        cerr << "ct dim is 0" << '\n';
        return message;
    }
    unsigned char *pPt = new unsigned char[message.ct_t];
    int len, pt_len, ret;

    if (!(ctx = EVP_CIPHER_CTX_new())) {// Create and initialise the context
        cerr << "Impossible open cipher ctx" << "\n";
        return message;
    }

    if (!EVP_DecryptInit(ctx, EVP_aes_128_gcm(), key.data(), message.IV.data())) {
        cerr << "Error during the init" << "\n";
        return message;
    }

    if (!EVP_DecryptUpdate(ctx, NULL, &len, message.counter.data(), sizeof(int))) {
        cerr << "Error during the update with pt" << "\n";
        return message;
    }

    if (!EVP_DecryptUpdate(ctx, NULL, &len, message.IV.data(), IV_DIM_GCM)) {
        cerr << "Error during the update with pt" << "\n";
        return message;
    }

    //Provide the message to be decrypted, and obtain the plaintext output.
    if (!EVP_DecryptUpdate(ctx, pPt, &len, message.ct.data(), message.ct_t)) {
        cerr << "Error during the update with ct" << "\n";
        return message;
    }
    pt_len = len;

    if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_TAG, TAGSIZE, message.tag.data())) {  // Set expected tag value.
        cerr << "Error during the check of TAG" << "\n";
        return message;
    }

    ret = EVP_DecryptFinal(ctx, pPt + len, &len); //Finalise the decryption.

    EVP_CIPHER_CTX_cleanup(ctx);// Clean up

    if (ret < 0) {
        cerr << "Decrypt was failed" << "\n";
        delete[] pPt;
        return message;
    }
    pt_len += len; // Success

    message.setPt(pPt, pt_len);
    delete[] pPt;

    return message;
}

Message EncryptByGCM(unsigned char *pPt, size_t pt_t, vector<unsigned char> key, size_t counter) {
    vector<unsigned char> tag(TAGSIZE);
    vector<unsigned char> ct(pt_t);
    int len = 0;
    int ciphertext_len;
    Message message;
    EVP_CIPHER_CTX *ctx;

    auto *char_counter = new unsigned char[sizeof(int)];
    memcpy(char_counter, &counter, sizeof(int));

    const EVP_CIPHER *cipher = EVP_aes_128_gcm();
    int iv_len = EVP_CIPHER_iv_length(cipher);
    vector<unsigned char> iv = GenerateNonceIV(iv_len);

    if (!(ctx = EVP_CIPHER_CTX_new())) {  // Create and initialise the context
        cerr << "Impossible open cipher ctx" << "\n";
        return message;
    }
    // Initialise the encryption operation.
    if (EVP_EncryptInit(ctx, EVP_aes_128_gcm(), key.data(), iv.data()) != 1) {
        cerr << "Error during the init" << "\n";
        return message;
    }
    //add only for tag check
    //add counter
    if (EVP_EncryptUpdate(ctx, NULL, &len, char_counter, sizeof(int)) != 1) {
        cerr << "Error during the update with pt" << "\n";
        return message;
    }
    //add IV
    if (EVP_EncryptUpdate(ctx, NULL, &len, iv.data(), IV_DIM_GCM) != 1) {
        cerr << "Error during the update with pt" << "\n";
        return message;
    }
    //add PT
    if (EVP_EncryptUpdate(ctx, &ct[0], &len, pPt, pt_t) != 1) {
        cerr << "Error during the update with pt" << "\n";
        return message;
    }
    ciphertext_len = len;

    if (EVP_EncryptFinal(ctx, &ct[0] + len, &len) != 1) {  //Finalize Encryption
        cerr << "Decrypt was failed" << "\n";
        return message;
    }
    ciphertext_len += len;

    if (EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_GET_TAG, TAGSIZE, &tag[0]) != 1) {  //Get the tag
        cerr << "Error during creation of TAG" << "\n";
        return message;
    }

    EVP_CIPHER_CTX_free(ctx); // Clean up

    message.setPt(pPt, pt_t);
    message.setCt(ct, ciphertext_len);
    message.setIV(iv, iv_len);
    message.setCounter(char_counter);
    message.setTag(tag, TAGSIZE);

    return message;
}

vector<unsigned char> GenerateVectorToSendMessage(Message message) {
    vector<unsigned char> v;
    if (message.ct.empty() || message.tag.empty() || message.IV.empty()) {
        cerr << "Encrypted message is empty" << "\n";
        return v;
    }
    v.insert(v.begin(), message.tag.begin(), message.tag.end());
    v.insert(v.end(), message.counter.begin(), message.counter.end());
    v.insert(v.end(), message.IV.begin(), message.IV.end());
    v.insert(v.end(), message.ct.begin(), message.ct.end());
    return v;
}

int WaitResponse(Session *pSession, int encry_flag, int sign_flag) {
    vector<unsigned char> type;
    Payload payload;

    cout << "Wait response..." << '\n';

    Message message = ReceiveMessage(pSession, encry_flag, sign_flag);
    if (message.ct.empty()) {
        cerr << "Error when try to create message" << "\n";
        return FAIL;
    }
    if (encry_flag == GCM_MESSAGE) { //IF THE MESSAGE IS CRYPTED DECRYPT IT
        message = DecryptMessage(message, pSession->session_key, pSession);
        if (message.ct.empty() || message.pt.empty()) {
            cerr << "Error when try to create message" << "\n";
            return FAIL;
        }
    } else if (encry_flag != NO_CRIPT_MESSAGE) {
        cerr << "Invalid encrypt flag" << "\n";
        return FAIL;
    }

    if (payload.setPeyload(message.pt) < 0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    if (payload.type.empty()) {
        cerr << "Error when try to get attribute of message" << "\n";
        return FAIL;
    }

    if (VerifyTypeMessage(payload.type, OK_TYPE) > 0) {
        cout << "Successfully received message" << "\n";
    } else if (VerifyTypeMessage(payload.type, ERROR_TYPE) > 0) {
        payload.response.push_back('\0');
        cerr << "\nYou Received an error message: " << payload.response.data() << "\n";
        return FAIL;
    } else {
        cerr << "Generic error when it try to get type of message";
        return FAIL;
    }

    return SUCCEED;
}

Message GenerateMessageByCt(vector<unsigned char> ct, size_t ct_t) {
    const EVP_CIPHER *cipher = EVP_aes_128_gcm();
    int iv_len = EVP_CIPHER_iv_length(cipher);
    int ct_len = ct_t - iv_len - TAGSIZE - sizeof(int);
    int z = 0;
    Message message;
    vector<unsigned char> temp(TAGSIZE);

    for (int i = 0; i < TAGSIZE; i++, z++) {
        temp[i] = ct.at(z);
    }
    message.setTag(temp, TAGSIZE);
    temp.clear();

    temp.resize(sizeof(int));
    for (int i = 0; i < sizeof(int); i++, z++) {
        temp[i] = ct.at(z);
    }
    message.setCounter(temp.data());
    temp.clear();

    temp.resize(iv_len);
    for (int i = 0; i < iv_len; i++, z++) {
        temp[i] = ct.at(z);
    }
    message.setIV(temp, iv_len);
    temp.clear();

    temp.resize(ct_len);
    for (int i = 0; i < ct_len; i++, z++) {
        temp[i] = ct.at(z);
    }
    message.setCt(temp, ct_len);

    return message;
}

vector<unsigned char>
EncryptMessage(unsigned char *pPt, size_t pt_t, vector<unsigned char> session_key, Session *pSession) {
    vector<unsigned char> v;
    pSession->friend_counterGCM++;
    Message message = EncryptByGCM(pPt, pt_t, std::move(session_key), pSession->friend_counterGCM);

    if (message.ct.empty() || message.tag.empty() || message.IV.empty() || message.counter.empty()) {
        cerr << "Encrypted message is empty" << "\n";
        return v;
    }
    v = GenerateVectorToSendMessage(message);
    if (v.empty()) {
        cerr << "Error when try to generate message" << "\n";
        return vector<unsigned char>();
    }
    return v;
}

Message DecryptMessage(Message message, vector<unsigned char> session_key, Session *pSession) {
    Message msg = GenerateMessageByCt(message.ct, message.ct_t);
    if (msg.ct.empty() || msg.tag.empty() || msg.IV.empty() || msg.counter.empty()) {
        cerr << "Decrypted message is empty" << "\n";
        return msg;
    }

    int counter;
    memcpy(reinterpret_cast<void *>(&counter), msg.counter.data(), sizeof(int));
    if (counter < pSession->my_counterGSM) {
        cerr << "Counter num is not valid, possible reply attack" << "\n";
        return Message();
    } else {
        pSession->my_counterGSM++;
    }
    return DecryptByGCM(msg, std::move(session_key));
}

vector<unsigned char> SignMessage(EVP_PKEY *pPrivkey, vector<unsigned char> v, size_t dim) {
    int ret = 0; // used for return values
    unsigned int uns_sign_len;
    vector<unsigned char> sign(EVP_PKEY_size(pPrivkey));

    EVP_MD_CTX *pCtx = EVP_MD_CTX_new();
    if (!pCtx) {
        cerr << "Error: impossible create context " << ret << "\n";
        return sign;
    }
    ret = EVP_SignInit(pCtx, EVP_sha256());
    if (ret == 0) {
        cerr << "Error: EVP_SignInit returned " << ret << "\n";
        return sign;
    }
    ret = EVP_SignUpdate(pCtx, v.data(), dim);
    if (ret == 0) {
        cerr << "Error: EVP_SignUpdate returned " << ret << "\n";
        return sign;
    }
    ret = EVP_SignFinal(pCtx, sign.data(), &uns_sign_len, pPrivkey);
    if (ret == 0) {
        cerr << "Error: EVP_SignFinal returned " << ret << "\n";
        return sign;
    }

    cout << "message is ready to send \n";

    EVP_MD_CTX_free(pCtx);
    return sign;

}

int VerifySing(EVP_PKEY *pPubKey, vector<unsigned char> buf, size_t dim, vector<unsigned char> sign) {
    int ret = 0; // used for return values
    EVP_MD_CTX *pCtx = EVP_MD_CTX_new();

    if (!pPubKey) {
        cerr << "Error:Public key is null " << "\n";
        goto err;
    }
    if (!pCtx) {
        cerr << "Error: impossible create context " << ret << "\n";
        goto err;
    }
    ret = EVP_VerifyInit(pCtx, EVP_sha256());
    if (ret <= 0) {
        cerr << "Error: EVP_VerifyInit returned " << ret << "\n";
        goto err;
    }
    ret = EVP_VerifyUpdate(pCtx, buf.data(), dim);
    if (ret <= 0) {
        cerr << "Error: EVP_VerifyUpdate returned " << ret << "\n";
        goto err;
    }
    ret = EVP_VerifyFinal(pCtx, sign.data(), SIGNATURELEN, pPubKey);
    if (ret <= 0) {
        cerr << "Error: EVP_VerifyFinal returned " << ret << "\n";
        goto err;
    }

    EVP_MD_CTX_free(pCtx);
    return ret;

    err:
    EVP_MD_CTX_free(pCtx);
    return FAIL;
}

int SetSessionNonce(Payload *payload, Session *pSession) {
    int nonce;

    if (payload->friend_nonce.empty()) {
        cerr << "friend nonce is empty" << "\n";
        return FAIL;
    }

    memcpy(reinterpret_cast<void *>(&nonce), payload->friend_nonce.data(), sizeof(int));
    pSession->friend_nonce = nonce;

    return SUCCEED;
}

int VerifyNonce(vector<unsigned char> nonce, int type, Session *pSession) {
    int int_nonce;
    if (nonce.empty()) {
        cerr << "nonce is empty" << '\n';
        return FAIL;
    }
    memcpy(reinterpret_cast<void *>(&int_nonce), nonce.data(), sizeof(int));

    switch (type) {
        case FRIEND_NONCE:
            if (int_nonce == pSession->friend_nonce) {
                return SUCCEED;
            }
            break;
        case MY_NONCE:
            if (int_nonce == pSession->my_nonce) {
                return SUCCEED;
            }
            break;
        default:
            cerr << "type nonce error" << '\n';
    }
    cerr << "Error to autenticate,nonce is different" << "\n";
    return FAIL;
}

int VerifyTypeMessage(vector<unsigned char> message_type, int type) {
    int int_type_message;
    if (message_type.empty()) {
        cerr << "nonce is empty" << '\n';
        return FAIL;
    }
    memcpy(reinterpret_cast<void *>(&int_type_message), message_type.data(), sizeof(int));

    if (int_type_message == type) {
        return SUCCEED;
    }

    return FAIL;
}

Message ReceiveMessage(Session *pSession, int encry_flag, int sign_flag) {
    int bytes;
    Message message;
    vector<unsigned char> buff(BUFFER);
    vector<unsigned char> signature;
    vector<unsigned char> temp_message;


    bytes = recv(pSession->session_id, buff.data(), BUFFER, 0);
    if (bytes <= 0 || buff.empty()) {
        cerr << "Error:Generic error to receive message: Bytes are: " << bytes << "\n";
        return message;
    }
    //check if buffer is full, so possible error
    if (buff.at(BUFFER - 1) != '\0') {
        cerr << "Error:Message is too big: Bytes are: " << bytes << "\n";
        return message;
    }

    //Set signature
    if (sign_flag == CHECK_SIGN) {
        signature.insert(signature.begin(), buff.begin(), buff.begin() + SIGNATURELEN);
        message.setSignature(signature, SIGNATURELEN);
    }

    //Set payload
    temp_message.insert(temp_message.begin(), buff.begin() + signature.size(), buff.end());
    message.setCt(temp_message, temp_message.size());

    if (encry_flag == NO_CRIPT_MESSAGE) {
        message.setPt(temp_message, temp_message.size());
    }

    if (sign_flag == CHECK_SIGN) {
        if (VerifySing(pSession->pPub_key, message.ct, message.ct_t, message.signature) < 0) {
            cerr << "Error: signature is invalid" << "\n";
            return Message();
        }
        cout << "verified signature" << '\n';
    } else if (sign_flag != NO_CHECK_SIGN) {
        cerr << "Invalid sign flag" << "\n";
        return Message();
    }
    return message;
}

int SendMessage(int session, vector<unsigned char> v, size_t dim, EVP_PKEY *pKey, int sign_flag) {
    int ret;
    vector<unsigned char> msg;

    if (sign_flag == CHECK_SIGN) {//SIGN MESSAGE
        v.resize(BUFFER - SIGNATURELEN);
        vector<unsigned char> signature = SignMessage(pKey, v, v.size());
        if (signature.empty()) {
            cerr << "Error:error to sign the message " << "\n";
            return FAIL;
        }
        msg.insert(msg.begin(), signature.begin(), signature.end());
    }
    msg.insert(msg.end(), v.begin(), v.end());

    ret = send(session, msg.data(), msg.size(), 0);
    if (ret <= 0) {
        cout << "error to send ct" << "\n";
        return FAIL;
    }


    return SUCCEED;
}

int SendError(Session *pSession, EVP_PKEY *pKey, char *err, int encry_flag, int sign_flag) {
    vector<unsigned char> message;
    Payload payload;

    payload.setType(ERROR_TYPE, SEND_MSG);
    if (payload.setResponse(reinterpret_cast<unsigned char *>(err), strnlen(err, RESPONSE_ATTRIBUTE_DIM), SEND_MSG) <
        0) {
        cerr << "Error impossible set payload " << "\n";
        return FAIL;
    }

    message = payload.getPayload();

    if (encry_flag == GCM_MESSAGE) {
        message = EncryptMessage(message.data(), message.size(), pSession->session_key, pSession);
    }
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    if (SendMessage(pSession->session_id, message, message.size(), pKey, sign_flag) < 0) {
        cerr << "Error:To send error message " << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int SendOk(Session *pSession, EVP_PKEY *pKey, int encry_flag, int sign_flag) {
    vector<unsigned char> message;
    Payload payload;

    payload.setType(OK_TYPE, SEND_MSG);
    message = payload.getPayload();

    if (encry_flag == GCM_MESSAGE) {
        message = EncryptMessage(message.data(), message.size(), pSession->session_key, pSession);
    }
    if (message.empty()) {
        cerr << "Message is empty" << "\n";
        return FAIL;
    }

    cout << "try to send ok message " << "\n";
    if (SendMessage(pSession->session_id, message, message.size(), pKey, sign_flag) < 0) {
        cerr << "Error:To send ok message " << "\n";
        return FAIL;
    }

    return SUCCEED;
}

int ReceiveCertificate(Session *pSession, Client *pClient) {
    Message rec_message;
    unsigned char *pCert_buf;

    rec_message = ReceiveMessage(pSession, NO_CRIPT_MESSAGE, NO_CHECK_SIGN);
    if (rec_message.pt_t == 0 || rec_message.pt.empty()) {
        cerr << "pt dim is 0" << '\n';
        return FAIL;
    }
    pCert_buf = new unsigned char[rec_message.pt_t];
    memcpy(pCert_buf, rec_message.pt.data(), rec_message.pt_t);

    //DESERIALIZE SERVER CERTIFICATE & FREE buf
    X509 *pCert = d2i_X509(NULL, (const unsigned char **) &pCert_buf, rec_message.pt_t);
    if (!pCert) {
        cerr << "Error: error to deserialize the certificate, impossible complete the connection " << "\n";
        return FAIL;
    }

    //VERIFY CERTIFICATE
    if (VerifyCertificate(pClient->store, pCert) < 0) { //VERIFY SERVER CERTIFICATE
        cerr << "Error certificate is invalid " << "\n";
        return FAIL;
    }

    //set only pub key of server after checking the certificate
    pSession->pPub_key = GetPublicKey(pCert);

    return SUCCEED;
}

int VerifyCertificate(X509_STORE *pStore, X509 *pCert) {
    X509_STORE_CTX *pCtx;
    int validation;

    pCtx = X509_STORE_CTX_new();
    if (!pCtx) {
        cerr << "ERROR to open X509_ctx" << "\n";
        goto err;
    }
    if (X509_STORE_CTX_init(pCtx, pStore, pCert, NULL) < 0) {
        cerr << "ERROR to init X509_ctx" << "\n";
        goto err;
    }

    validation = X509_verify_cert(pCtx);

    if (validation != 1) {
        cerr << "invalid certificate" << "\n";
        X509_STORE_CTX_free(pCtx);
        goto err;
    }
    cout << "cert server valid" << "\n";
    X509_STORE_CTX_free(pCtx);
    return SUCCEED;

    err:
    return FAIL;
}

EVP_PKEY *GetPublicKey(X509 *cert) {
    EVP_PKEY *pPubKey = X509_get_pubkey(cert);
    if (!pPubKey) {
        std::cerr << "It's not possible axtract the public key" << "\n";
        return NULL;
    }
    return pPubKey;
}

int CompareVector(vector<unsigned char> v, const char *c) {
    int c_len = strnlen(c, BUFFER);
    int v_len = v.size();
    for (int i = 0; v.at(i) != '\0' && v.at(i) == c[i] && i <= v_len; i++) {
        if (i == c_len - 1) {
            return SUCCEED;
        }
    }
    return FAIL;
}

int GenNonce() {
    int r;
    RAND_poll();
    int rc = RAND_bytes(reinterpret_cast<unsigned char *>(&r), sizeof(int));
    if (rc != 1) {
        fprintf(stderr, "error");
        return 0;
    }
    return r;
}

vector<unsigned char> GenerateRandomSequece(size_t dimension) {
    vector<unsigned char> v(dimension);
    RAND_poll();
    RAND_bytes(&v[0], dimension);
    return v;
}

//FIRST N-4 CARACTERS ARE CASUAL, LAST 4 ARE DETERMINISTIC(SEQUENCE)
vector<unsigned char> GenerateNonceIV(size_t dimension) {
    auto *nonce = new unsigned char[4];
    memcpy(nonce, &IV_nonce, sizeof(int));
    vector<unsigned char> IV = GenerateRandomSequece(dimension - sizeof(int));
    IV.insert(IV.end(), nonce, nonce + sizeof(int));

    delete[] nonce;
    IV_nonce++;
    return IV;
}




