//
// Created by Federico Cosimo Lapenna on 09/06/2020.
//

#include <iostream>
#include <openssl/ossl_typ.h>
#include <cstring>
#include <vector>
#include <openssl/pem.h>
#include "../model/Message.h"
#include "../typeMessage.h"
#include "../model/Session.h"
#include "../operations/Operations.h"

# define BUFFER 1024

using namespace std;

static DH *get_dh2048(void) {
    static unsigned char dhp_2048[] = {
            0xBF, 0x21, 0x9F, 0x8C, 0x5A, 0x46, 0x9B, 0xCF, 0xFF, 0x78,
            0x73, 0x82, 0xFE, 0x95, 0x4E, 0xF9, 0x89, 0x8F, 0xCA, 0x83,
            0xBD, 0x4D, 0xB8, 0x16, 0xED, 0x1F, 0xAE, 0x3F, 0x4C, 0x66,
            0x23, 0xC6, 0x49, 0x2C, 0x18, 0xC7, 0xEA, 0x3B, 0x36, 0xE5,
            0x7C, 0xB7, 0xB3, 0xB6, 0xEE, 0x26, 0x89, 0xEF, 0x3A, 0xD6,
            0xF1, 0xCD, 0xD5, 0x71, 0xBF, 0x9C, 0x74, 0xDA, 0xE0, 0x99,
            0x54, 0xDC, 0x4A, 0x60, 0x50, 0x08, 0x05, 0xB5, 0x78, 0x22,
            0xC9, 0x0A, 0x49, 0xB0, 0xDA, 0x76, 0x09, 0x55, 0x12, 0x5D,
            0x35, 0xA8, 0x31, 0xAA, 0xAF, 0xD9, 0xD3, 0xC6, 0xCD, 0xA7,
            0x18, 0x26, 0xC6, 0xA6, 0xFC, 0x58, 0xAB, 0xD3, 0xDA, 0x7F,
            0x7B, 0x43, 0x66, 0x04, 0x84, 0x29, 0xA7, 0x61, 0xB9, 0xF2,
            0x60, 0x9E, 0x85, 0x42, 0x13, 0xE9, 0xA8, 0x56, 0x91, 0x84,
            0x77, 0x62, 0x97, 0xDE, 0xC3, 0x8B, 0xB4, 0x86, 0xCD, 0x96,
            0x46, 0x43, 0x22, 0xC8, 0x10, 0x5F, 0x67, 0x25, 0x33, 0xBD,
            0x37, 0x70, 0xB8, 0x99, 0xA7, 0x98, 0x6E, 0x7E, 0x89, 0xE5,
            0x0C, 0x8F, 0xE4, 0x4B, 0x5E, 0xC8, 0xB8, 0x2F, 0x4B, 0xFE,
            0xAE, 0xE2, 0xA1, 0x0E, 0xB9, 0xDC, 0xF3, 0x47, 0xAD, 0xE0,
            0x81, 0xA3, 0x0E, 0x2E, 0x7E, 0x13, 0x10, 0x4E, 0x7B, 0x51,
            0x75, 0x53, 0xB2, 0xC3, 0xA5, 0x95, 0x67, 0x91, 0x95, 0xB5,
            0x34, 0xDC, 0x25, 0xE2, 0xBF, 0x67, 0x0E, 0xB2, 0x5E, 0x37,
            0xB4, 0x89, 0xF1, 0xA7, 0x1B, 0x20, 0x0C, 0x29, 0xD2, 0x26,
            0x3D, 0xDC, 0x03, 0xFF, 0x4F, 0x99, 0x92, 0x81, 0x02, 0x37,
            0x65, 0xD8, 0x8C, 0xF6, 0xD1, 0x96, 0x9D, 0xE5, 0x2E, 0x99,
            0xE3, 0x7A, 0x95, 0xAE, 0x52, 0xCB, 0x11, 0x7F, 0xA6, 0xBD,
            0xB3, 0xAE, 0xD2, 0xA7, 0xED, 0xD6, 0xF1, 0x10, 0xBA, 0x4F,
            0xE2, 0x90, 0x4B, 0x48, 0x91, 0x13
    };
    static unsigned char dhg_2048[] = {
            0x02
    };
    DH *dh = DH_new();
    BIGNUM *p, *g;

    if (dh == NULL)
        return NULL;
    p = BN_bin2bn(dhp_2048, sizeof(dhp_2048), NULL);
    g = BN_bin2bn(dhg_2048, sizeof(dhg_2048), NULL);
    if (p == NULL || g == NULL
        || !DH_set0_pqg(dh, p, NULL, g)) {
        DH_free(dh);
        BN_free(p);
        BN_free(g);
        return NULL;
    }
    return dh;
}

//Get serialized key and return EVP_PKEY
EVP_PKEY *getPubKeyEVP_PKEY(unsigned char *pKey) {
    EVP_PKEY *pPub_key = EVP_PKEY_new();
    if (!pPub_key) {
        cerr << "Error to initialize PKEY" << '\n';
        return NULL;
    }
    // write char array to BIO
    BIO *pPublicBIO = BIO_new_mem_buf(pKey, -1);
    if (!pPublicBIO) {
        cerr << "Bio is empty" << '\n';
        return NULL;
    }
    // create a RSA object from public key char array
    EVP_PKEY *publicKey = NULL;
    PEM_read_bio_PUBKEY(pPublicBIO, &publicKey, NULL, NULL);
    // create public key
    //EVP_PKEY_assign(pPub_key, rsaPublicKey);

    return publicKey;
}

int GenEphemeralKey(EVP_PKEY **my_dhkey) {
    /*GENERATING MY EPHEMERAL KEY*/
/* Use built-in parameters */
    printf("Start: loading standard DH parameters\n");
    EVP_PKEY *params = EVP_PKEY_new();

    if (params == NULL) {
        cerr << "EVP param is null" << '\n';
        return FAIL;
    }

    DH *temp = get_dh2048();

    if (EVP_PKEY_set1_DH(params, temp) != 1) {
        cerr << "EVP set DH error" << '\n';
        return FAIL;
    }
    DH_free(temp);
    cout << "Generating ephemeral DH KeyPair" << '\n';

/* Create context for the key generation */
    EVP_PKEY_CTX *DHctx = EVP_PKEY_CTX_new(params, NULL);
    if (!DHctx) {
        cerr << "EVP CTX is null" << '\n';
        return FAIL;
    }

/* Generate a new key */
//    EVP_PKEY *my_dhkey = NULL;
    if (EVP_PKEY_keygen_init(DHctx) != 1) {
        cerr << "EVP keygen init error" << '\n';
        return FAIL;
    }
    if (EVP_PKEY_keygen(DHctx, my_dhkey) != 1) {
        cerr << "Error: impossible set dh key" << '\n';
        return FAIL;
    }

    EVP_PKEY_CTX_free(DHctx);
    EVP_PKEY_free(params);
    return SUCCEED;
}

vector<unsigned char> GetSharedSecret(EVP_PKEY *peer_pubkey, EVP_PKEY *my_dhkey) {
    vector<unsigned char> skey;

    printf("Deriving a shared secret\n");
/*creating a context, the buffer for the shared key and an int for its length*/
    EVP_PKEY_CTX *derive_ctx;
    //unsigned char *skey;
    size_t skeylen;
    derive_ctx = EVP_PKEY_CTX_new(my_dhkey, NULL);
    if (!derive_ctx) {
        cerr << "error to set context" << '\n';
        return skey;
    }

    if (EVP_PKEY_derive_init(derive_ctx) <= 0) {
        cerr << "error to init derive" << '\n';
        return skey;
    }

/*Setting the peer with its pubkey*/
    if (EVP_PKEY_derive_set_peer(derive_ctx, peer_pubkey) <= 0) {
        cerr << "error to derive set peer" << '\n';
        return skey;

    }
/* Determine buffer length, by performing a derivation but writing the result nowhere */
    EVP_PKEY_derive(derive_ctx, NULL, &skeylen);
    if (skeylen <= 0) {
        cerr << "len derive key is: " << skeylen << '\n';
        return skey;
    }

/*allocate buffer for the shared secret*/
    skey.resize(skeylen);

/*Perform again the derivation and store it in skey buffer*/
    if (EVP_PKEY_derive(derive_ctx, &skey[0], &skeylen) <= 0) {
        cerr << "error to derive key" << '\n';
        return skey;
    }
    EVP_PKEY_CTX_free(derive_ctx);

    return skey;
}

vector<unsigned char> hashingElement(vector<unsigned char> skey) {
    vector<unsigned char> digest(EVP_MD_size(EVP_sha256()));
    unsigned int digestlen;

// Create and init context
    EVP_MD_CTX *Hctx = EVP_MD_CTX_new();
    if (!Hctx) {
        cerr << "Error: Impossible create context" << '\n';
        return vector<unsigned char>();
    }

    if (EVP_DigestInit(Hctx, EVP_sha256()) != 1) {
        cerr << "Error: Fail digest Init" << '\n';
        return vector<unsigned char>();
    }

    if (EVP_DigestUpdate(Hctx, skey.data(), skey.size()) != 1) {
        cerr << "Error: Fail Digest Update" << '\n';
        return vector<unsigned char>();
    }

    if (EVP_DigestFinal(Hctx, &digest[0], &digestlen) != 1) {
        cerr << "Error: Fail digest Final" << '\n';
        return vector<unsigned char>();
    }

    EVP_MD_CTX_free(Hctx);

    return digest;
}

vector<unsigned char> getDHCommonKey(Session *pSession, EVP_PKEY *privKey) {
    vector<unsigned char> pub_key;
    vector<unsigned char> message;
    vector<unsigned char> skey;
    EVP_PKEY *pkey = NULL;

    if (GenEphemeralKey(&pkey) < 0) {
        cerr << "Error to generate d-h ephemeral key" << "\n";
        return skey;
    }

    //EXTRACT PUB KEY
    pub_key = getPubKey(pkey);
    if (pub_key.empty()) {
        cerr << "Error pub key is empty" << "\n";
        return skey;
    }

    unsigned char *friend_nonce = new unsigned char[4];
    memcpy(friend_nonce, &pSession->friend_nonce, sizeof(int));

    //set nonce in the message
    message.insert(message.begin(), friend_nonce, friend_nonce + sizeof(int));
    //set my pub key in the message
    message.insert(message.end(), pub_key.begin(), pub_key.end());

    if (SendMessage(pSession->session_id, message, message.size(), privKey, CHECK_SIGN) < 0) {
        cerr << "Error to send pub key" << "\n";
        return skey;
    }

    cout << "Wait friend pub key..." << '\n';

    Message rec_message = ReceiveMessage(pSession, NO_CRIPT_MESSAGE, CHECK_SIGN);
    if (rec_message.ct.empty()) {
        cerr << "Error to OK message, Negotation is failed " << "\n";
        return skey;
    }

    vector<unsigned char> my_nonce;

    //get nonce and pubkey of friend
    my_nonce.insert(my_nonce.begin(), rec_message.pt.data(), rec_message.pt.data() + sizeof(int));

    if (VerifyNonce(my_nonce, MY_NONCE, pSession) < 0) {
        cerr << "Nonce is not fresh" << "\n";
        return skey;
    }

    pub_key.clear();
    pub_key.insert(pub_key.begin(), rec_message.pt.data() + sizeof(int), rec_message.pt.data() + rec_message.pt_t);

    EVP_PKEY *peer_pubkey = getPubKeyEVP_PKEY(pub_key.data());
    if (peer_pubkey == NULL) {
        cerr << "error: peer pubkey is null" << "\n";
        return skey;
    }

    skey = GetSharedSecret(peer_pubkey, pkey);
    if (skey.empty()) {
        cerr << "shared secret is null" << "\n";
        return skey;
    }


    vector<unsigned char> secure_key = hashingElement(skey);
    if (secure_key.empty()) {
        return secure_key;
    }

    EVP_PKEY_free(peer_pubkey);
    EVP_PKEY_free(pkey);
    return secure_key;

}


