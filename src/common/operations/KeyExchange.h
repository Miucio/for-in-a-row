//
// Created by Federico Cosimo Lapenna on 09/06/2020.
//

#ifndef CLIENTPROJECTSECURITY_KEYEXCHANGE_H
#define CLIENTPROJECTSECURITY_KEYEXCHANGE_H

#include <vector>
#include <openssl/ossl_typ.h>
#include "../model/Message.h"

using namespace std;

EVP_PKEY *getPubKeyEVP_PKEY(unsigned char *pKey);

vector<unsigned char> GetSharedSecret(EVP_PKEY *peer_pubkey, EVP_PKEY *my_dhkey);

vector<unsigned char> getDHCommonKey(Session *pSession, EVP_PKEY *privKey);

#endif //CLIENTPROJECTSECURITY_KEYEXCHANGE_H
