//
// Created by Federico Cosimo Lapenna on 26/06/2020.
//

#include <sstream>
#include <iostream>
#include "Payload.h"

Payload::Payload() {
    type.resize(INT_ATTRIBUTE_DIM, '#');
    my_nonce.resize(INT_ATTRIBUTE_DIM, '#');
    friend_nonce.resize(INT_ATTRIBUTE_DIM, '#');
    response.resize(RESPONSE_ATTRIBUTE_DIM, '#');
    address.resize(ADDRESS_ATTRIBUTE_DIM, '#');
    port.resize(INT_ATTRIBUTE_DIM, '#');
    user.resize(USER_NAME_SIZE_MAX, '#');
    game_move.resize(INT_ATTRIBUTE_DIM, '#');
    key.resize(SESSIONKEYSIZE, '#');
    users_size.resize(INT_ATTRIBUTE_DIM);
}

void Payload::setType(int type, int flag) {
    if (type == 0 && flag == RCV_MSG) {
        Payload::type.clear();
        return;
    }
    Payload::type.resize(INT_ATTRIBUTE_DIM);
    memcpy(Payload::type.data(), &type, INT_ATTRIBUTE_DIM);
}

void Payload::setMyNonce(int nonce_a, int flag) {
    if (nonce_a == 0 && flag == RCV_MSG) {
        Payload::friend_nonce.clear();
        return;
    }

    Payload::friend_nonce.resize(INT_ATTRIBUTE_DIM);
    memcpy(Payload::friend_nonce.data(), &nonce_a, INT_ATTRIBUTE_DIM);
}

void Payload::setFriendNonce(int nonce_b, int flag) {
    if (nonce_b == 0 && flag == RCV_MSG) {
        Payload::my_nonce.clear();
        return;
    }

    Payload::my_nonce.resize(INT_ATTRIBUTE_DIM);
    memcpy(Payload::my_nonce.data(), &nonce_b, INT_ATTRIBUTE_DIM);
}

int Payload::setResponse(unsigned char *response, size_t dim, int flag) {
    if (dim > RESPONSE_ATTRIBUTE_DIM || dim < 0) {
        std::cerr << "Impossible set Response in the payload" << '\n';
        return FAIL;
    }
    switch (flag) {
        case SEND_MSG:
            Payload::response.assign(response, response + dim);
            Payload::response.resize(RESPONSE_ATTRIBUTE_DIM, '#');
            break;

        case RCV_MSG:
            if (dim == 0) {
                Payload::response.clear();
                break;
            }
            Payload::response.insert(Payload::response.begin(), response, response + dim);
            Payload::response.push_back('\0');
            Payload::response.resize(dim);
            break;
        default:
            return FAIL;
    }

    return SUCCEED;
}

int Payload::setAddress(unsigned char *address, size_t dim, int flag) {
    if (dim > ADDRESS_ATTRIBUTE_DIM || dim < 0) {
        std::cerr << "Impossible set address in the payload" << '\n';
        return FAIL;
    }
    switch (flag) {
        case SEND_MSG:
            Payload::address.assign(address, address + dim);
            Payload::address.resize(ADDRESS_ATTRIBUTE_DIM, '#');
            break;

        case RCV_MSG:
            if (dim == 0) {
                Payload::address.clear();
                break;
            }
            Payload::address.insert(Payload::address.begin(), address, address + dim);
            Payload::address.resize(dim);
            break;
        default:
            return FAIL;
    }

    return SUCCEED;
}

void Payload::setPort(int port, int flag) {
    if (port == 0 && flag == RCV_MSG) {
        Payload::port.clear();
        return;
    }

    Payload::port.resize(INT_ATTRIBUTE_DIM);
    memcpy(Payload::port.data(), &port, INT_ATTRIBUTE_DIM);
}

int Payload::setUserName(unsigned char *username, size_t dim, int flag) {
    if (dim > USER_NAME_SIZE_MAX || dim < 0) {
        std::cerr << "Impossible set user_name in the payload" << '\n';
        return FAIL;
    }
    switch (flag) {
        case SEND_MSG:
            Payload::user.assign(username, username + dim);
            Payload::user.resize(USER_NAME_SIZE_MAX, '#');
            break;

        case RCV_MSG:
            if (dim == 0) {
                Payload::user.clear();
                break;
            }
            Payload::user.insert(Payload::user.begin(), username, username + dim);
            Payload::user.resize(dim);
            break;
        default:
            return FAIL;
    }

    return SUCCEED;
}

void Payload::setGameMove(int game_move) {
    Payload::game_move.resize(INT_ATTRIBUTE_DIM);
    memcpy(Payload::game_move.data(), &game_move, INT_ATTRIBUTE_DIM);
}

int Payload::setKey(unsigned char *key, size_t dim, int flag) {
    if (dim > SESSIONKEYSIZE || dim < 0) {
        std::cerr << "Impossible set key in the payload" << '\n';
        return FAIL;
    }
    switch (flag) {
        case SEND_MSG:
            Payload::key.assign(key, key + dim);
            Payload::key.resize(SESSIONKEYSIZE, '#');
            break;

        case RCV_MSG:
            if (dim == 0) {
                Payload::key.clear();
                break;
            }
            Payload::key.insert(Payload::key.begin(), key, key + dim);
            Payload::key.resize(dim);
            break;
        default:
            return FAIL;
    }

    return SUCCEED;
}

void Payload::setUsersSize(int users_size) {
    if (users_size == -1) {
        Payload::users_size.clear();
        return;
    }

    Payload::users_size.resize(INT_ATTRIBUTE_DIM);
    memcpy(Payload::users_size.data(), &users_size, INT_ATTRIBUTE_DIM);
}

void Payload::setUsers(vector<unsigned char> users) {
    if (users.empty()) {
        setUsersSize(-1);
    }
    Payload::users.insert(Payload::users.begin(), users.begin(), users.end());
    setUsersSize(users.size());
}

int Payload::setPeyload(vector<unsigned char> payload) {
    static char ok_chars[] = "qwertyuiopasdfghjklzxcvbnm"
                             "QWERTYUIOPASDFGHJKLZXCVBNM"
                             "1234567890_-.@|#";
    int size_user = 0;
    int begin_position = 0;
    int end_position;
    int temp_int = 0;
    vector<unsigned char> temp;

    end_position = INT_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    if (temp.at(0) == '#' && temp.at(1) == '#' && temp.at(2) == '#' && temp.at(3) == '#') {
        setType(temp_int, RCV_MSG);
    } else {
        memcpy(reinterpret_cast<void *>(&temp_int), temp.data(), sizeof(int));
        setType(temp_int, RCV_MSG);
    }

    temp.clear();
    temp_int = 0;

    begin_position = end_position;
    end_position = begin_position + INT_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    if (temp.at(0) == '#' && temp.at(1) == '#' && temp.at(2) == '#' && temp.at(3) == '#') {
        setFriendNonce(temp_int, RCV_MSG);
    } else {
        memcpy(reinterpret_cast<void *>(&temp_int), temp.data(), sizeof(int));
        setFriendNonce(temp_int, RCV_MSG);
    }

    temp.clear();
    temp_int = 0;

    begin_position = end_position;
    end_position = begin_position + INT_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    if (temp.at(0) == '#' && temp.at(1) == '#' && temp.at(2) == '#' && temp.at(3) == '#') {
        setMyNonce(0, RCV_MSG);
    } else {
        memcpy(reinterpret_cast<void *>(&temp_int), temp.data(), sizeof(int));
        setMyNonce(temp_int, RCV_MSG);
    }
    temp.clear();
    temp_int = 0;

    begin_position = end_position;
    end_position = begin_position + RESPONSE_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    setResponse(temp.data(), getEndVector(temp), RCV_MSG);
    temp.clear();

    begin_position = end_position;
    end_position = begin_position + ADDRESS_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    setAddress(temp.data(), getEndVector(temp), RCV_MSG);
    temp.clear();

    begin_position = end_position;
    end_position = begin_position + INT_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    if (temp.at(0) == '#' && temp.at(1) == '#' && temp.at(2) == '#' && temp.at(3) == '#') {
        setPort(0, RCV_MSG);
    } else {
        memcpy(reinterpret_cast<void *>(&temp_int), temp.data(), sizeof(int));
        setPort(temp_int, RCV_MSG);
    }

    temp.clear();
    temp_int = 0;

    begin_position = end_position;
    end_position = begin_position + USER_NAME_SIZE_MAX;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    setUserName(temp.data(), getEndVector(temp), RCV_MSG);
    temp.clear();

    begin_position = end_position;
    end_position = begin_position + INT_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    if (temp.at(0) == '#' && temp.at(1) == '#' && temp.at(2) == '#' && temp.at(3) == '#') {
        Payload::game_move.clear();
    } else {
        memcpy(reinterpret_cast<void *>(&temp_int), temp.data(), sizeof(int));
        setGameMove(temp_int);
    }

    temp.clear();
    temp_int = 0;

    begin_position = end_position;
    end_position = begin_position + SESSIONKEYSIZE;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    setKey(temp.data(), getEndVector(temp), RCV_MSG);
    temp.clear();

    begin_position = end_position;
    end_position = begin_position + INT_ATTRIBUTE_DIM;
    temp.insert(temp.begin(), payload.begin() + begin_position, payload.begin() + end_position);
    if (temp.at(0) == '\0') {
        setUsersSize(-1);
    } else {
        memcpy(reinterpret_cast<void *>(&size_user), temp.data(), sizeof(int));
        if (size_user > 500) {
            cerr << "user size is too big, close" << '\n';
            return FAIL;
        }
        setUsersSize(size_user);
    }
    temp.clear();
    temp_int = 0;

    if (size_user != 0) {
        begin_position = end_position;
        end_position = begin_position + size_user;
        users.assign(payload.begin() + begin_position, payload.begin() + end_position);
        if (strspn(reinterpret_cast<const char *>(users.data()), ok_chars) < size_user) { //check white list
            cerr << "user name contain invalid caractes" << '\n';
            return FAIL;
        }
    }

    return SUCCEED;
}

vector<unsigned char *> Payload::getUsersList() {
    vector<unsigned char *> users_list;
    vector<unsigned char> temp;
    unsigned char *temp_char;

    for (auto it = users.begin(); it != users.end(); it++) {
        if (*it == '|') {
            if (!temp.empty()) {
                temp.push_back('\0');
                temp_char = new unsigned char[temp.size()];
                memcpy(temp_char, temp.data(), temp.size());
                users_list.push_back(temp_char);
                temp.clear();
            }
        } else {
            temp.push_back(*it);
        }
    }
    return users_list;
}

vector<unsigned char> Payload::getPayload() {
    vector<unsigned char> payload;

    payload.insert(payload.begin(), type.begin(), type.end());
    payload.insert(payload.end(), my_nonce.begin(), my_nonce.end());
    payload.insert(payload.end(), friend_nonce.begin(), friend_nonce.end());
    payload.insert(payload.end(), response.begin(), response.end());
    payload.insert(payload.end(), address.begin(), address.end());
    payload.insert(payload.end(), port.begin(), port.end());
    payload.insert(payload.end(), user.begin(), user.end());
    payload.insert(payload.end(), game_move.begin(), game_move.end());
    payload.insert(payload.end(), key.begin(), key.end());
    payload.insert(payload.end(), users_size.begin(), users_size.end());
    if (!users.empty()) {
        payload.insert(payload.end(), users.begin(), users.end());
    }

    return payload;
}

size_t Payload::getEndVector(vector<unsigned char> v) {
    for (auto it = v.end() - 1; it != v.begin(); --it) {
        if (*it != '#') {
            return distance(v.begin(), it + 1);
        }
    }
    return 0;
}


