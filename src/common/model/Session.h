//
// Created by Federico Cosimo Lapenna on 28/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_SESSION_H
#define CLIENTPROJECTSECURITY_SESSION_H

#include <openssl/ossl_typ.h>
#include <vector>

class Session {
public:
    EVP_PKEY *pPub_key;
    int session_id = 0;
    int my_nonce = 0;
    int friend_nonce = 0;
    size_t my_counterGSM = 0;
    size_t friend_counterGCM = 0;
    unsigned char *user_name;
    size_t user_name_size = 0;
    std::vector<unsigned char> session_key;
    size_t session_key_t = 0;
    std::vector<unsigned char> address;
    size_t address_t = 0;
    int port = 0;
    int challenger_id = 0;
    bool busy = false;

    Session(int sessionId);

    Session();

    Session(const Session &obj);

    void setSessionKey(std::vector<unsigned char> session_key, size_t dim);

    void setUserName(std::vector<unsigned char> userName);

    void setAddress(std::vector<unsigned char> address, size_t dim);

    void cleanSessionKey();
};


#endif //CLIENTPROJECTSECURITY_SESSION_H
