//
// Created by Federico Cosimo Lapenna on 09/06/2020.
//

#include <vector>
#include <cstring>
#include "Message.h"

using namespace std;

#define BUFFER 1024

Message::Message() {}

void Message::setCt(vector<unsigned char> pCt, size_t dim) {
    Message::ct.assign(pCt.begin(), pCt.end());
    Message::ct.resize(dim);
    ct_t = dim;
}

void Message::setSignature(vector<unsigned char> pSignature, size_t dim) {
    Message::signature.assign(pSignature.begin(), pSignature.end());
    Message::signature.resize(dim);
    sign_t = dim;
}

void Message::setPt(vector<unsigned char> pPt, size_t dim) {
    Message::pt.assign(pPt.begin(), pPt.end());
    Message::pt.resize(dim);
    pt_t = dim;
}

void Message::setIV(vector<unsigned char> IV, size_t dim) {
    Message::IV.resize(dim);
    Message::IV.assign(IV.begin(), IV.end());
    IV_t = dim;
}

void Message::setTag(vector<unsigned char> tag, size_t dim) {
    Message::tag.assign(tag.begin(), tag.end());
    Message::tag.resize(dim);
    tag_t = dim;
}

void Message::setPt(unsigned char *pPt, size_t dim) {
    pt.clear();
    pt.insert(pt.begin(), pPt, pPt + dim);
    pt_t = dim;
}

void Message::setCounter(unsigned char *pCounter) {
    counter.clear();
    counter.insert(counter.begin(), pCounter, pCounter + sizeof(int));
}

Message::Message(const Message &obj) {
    pt_t = obj.pt_t;
    pt.assign(obj.pt.begin(), obj.pt.end());
    pt.resize(pt_t);

    ct_t = obj.ct_t;
    ct.assign(obj.ct.begin(), obj.ct.end());
    ct.resize(ct_t);

    sign_t = obj.sign_t;
    signature.assign(obj.signature.begin(), obj.signature.end());
    signature.resize(sign_t);

    IV_t = obj.IV_t;
    IV.assign(obj.IV.begin(), obj.IV.end());
    IV.resize(IV_t);

    encry_t = obj.encry_t;
    encrypted_key.assign(obj.encrypted_key.begin(), obj.encrypted_key.end());
    encrypted_key.resize(encry_t);

    tag_t = obj.tag_t;
    tag.assign(obj.tag.begin(), obj.tag.end());
    tag.resize(tag_t);

    counter = obj.counter;
    counter.assign(obj.counter.begin(), obj.counter.end());
    counter.resize(sizeof(int));

}

//vector<unsigned char> Message::getAttributeCharTest(const char *attribute) { ////lento con troppi cicli
//    int start, k, finish;
//    vector<unsigned char> ret;
//
//    int attribute_len = strnlen(attribute, BUFFER);
//    if (attribute_len <= 0) {
//        cerr << "Error: attribute is null " << "\n";
//    }
//
//    if (pt.empty()) {
//        std::cerr << "Error: message is null" << "\n";
//        goto err;
//    }
//
//    for (int i = 0; pt.at(i) != '\0'; i++) {
//        for (int j = 0; pt.at(i) == attribute[j]; i++, j++) {
//            if (j == attribute_len - 1) {
//                start = i + 1;
//                for (k = i + 1; pt.at(k) != '\0'; k++) {
//                    for (int q = 0; pt.at(k) == attribute[q]; k++, q++) {
//                        if (q == attribute_len - 1) {
//                            finish = k - attribute_len;
//                            goto ok;
//                        }
//                    }
//                }
//                i = k;
//            }
//        }
//    }
//
//    return ret;
//
//    err:
//    std::cerr << "Error:Don't find element" << "\n";
//    return ret;
//
//    ok:
//    vector<unsigned char>::const_iterator first = pt.begin() + start;
//    vector<unsigned char>::const_iterator last = pt.begin() + finish + 1;
//    vector<unsigned char> newVec(first, last);
//    return newVec;
//}

//char *Message::getAttribute(std::string attribute) {  ////NON SICURO PER CAST DA STRING A CHAR
//    size_t pos;
//    std::string temp;
//    char *pType;
//    std::string s;
//    if (pt.empty()) {
//        cerr << "Error: message is null " << "\n";
//        goto err;
//    }
//
//    cout << "Get attribute: " << attribute << "\n";
//    while ((pos = s.find(attribute)) != std::string::npos) {
//        temp = s.substr(0, pos);
//        s.erase(0, pos + attribute.length());
//    }
//    pType = strncpy(new char[temp.length() + 1], temp.c_str(), BUFFER);
//    if (pType == NULL) {
//        cerr << "Error: error to get type " << "\n";
//        goto err;
//    }
//    return pType;
//
//    err:
//    return nullptr;
//}

//char *Message::getAttributeChar(const char *attribute) {
//    __wrap_iter<unsigned char *> i = find(pt.begin(), pt.end(), 't');
//    char *temp = new char[BUFFER];
//    char *ret = new char[BUFFER];
//    char *out;
//    int attribute_len = strnlen(attribute, BUFFER);
//    if (attribute_len <= 0) {
//        cerr << "Error: attribute is null " << "\n";
//    }
//    if (!pt) {
//        std::cerr << "Error: message is null"  << "\n";
//        goto err;
//    }
//    strncpy(temp, pt, BUFFER);
//
//    out = strstr(temp, attribute);
//    if (!out) {
//        std::cerr << "Error to get attribute " << "pType: " << attribute << " message: " << pt << "\n";
//        goto err;
//    } else if(strncmp(out,temp,BUFFER)==0){
//        std::cerr << "Error to get attribute " << "pType: " << attribute << " message: " << pt << "\n";
//        goto err;
//    }
//    for (int i = 0 + attribute_len; out[i] != '\0'; i++) {
//        for (int j = 0; out[i] == attribute[j]; i++, j++) {
//            if (j == strlen(attribute) - 1) {
//                strncpy(ret, out, i + 1 - attribute_len);
//            }
//        }
//    }
//
//    delete []temp;
//    return ret+attribute_len;
//
//    err:
//    delete []temp;
//    return nullptr;
//}
//

