//
// Created by Federico Cosimo Lapenna on 09/06/2020.
//

#ifndef CLIENTPROJECTSECURITY_MESSAGE_H
#define CLIENTPROJECTSECURITY_MESSAGE_H

#include <vector>

using namespace std;

class Message {
public:
    vector<unsigned char> pt, ct, signature, IV, encrypted_key, tag, counter;
    size_t sign_t = 0;
    size_t ct_t = 0;
    size_t pt_t = 0;
    size_t IV_t = 0;
    size_t encry_t = 0;
    size_t tag_t = 0;

    Message();

    Message(const Message &obj);

    void setCt(vector<unsigned char> ct, size_t dim);

    void setPt(vector<unsigned char> pt, size_t dim);

    void setSignature(vector<unsigned char> signature, size_t dim);

    void setIV(vector<unsigned char> IV, size_t dim);

    void setTag(vector<unsigned char> tag, size_t dim);

    void setPt(unsigned char *pPt, size_t dim);

    void setCounter(unsigned char *pCounter);

//    vector<unsigned char> getAttributeCharTest(const char *attribute);
//
//    [[deprecated("not safe")]]
//    char *getAttributeChar(const char *attribute);
//
//    [[deprecated("not safe")]]
//    char *getAttribute(std::string attribute);

};


#endif //CLIENTPROJECTSECURITY_MESSAGE_H
