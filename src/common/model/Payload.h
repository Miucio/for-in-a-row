//
// Created by Federico Cosimo Lapenna on 26/06/2020.
//

#ifndef CLIENTPROJECTSECURITY_PAYLOAD_H
#define CLIENTPROJECTSECURITY_PAYLOAD_H

#include <vector>
#include "../typeMessage.h"

#define SEND_MSG 1
#define RCV_MSG 2

using namespace std;

class Payload {
public:
    Payload();

public:
    vector<unsigned char> type;
    vector<unsigned char> my_nonce;
    vector<unsigned char> friend_nonce;
    vector<unsigned char> response;
    vector<unsigned char> address;
    vector<unsigned char> port;
    vector<unsigned char> user;
    vector<unsigned char> game_move;
    vector<unsigned char> key;
    vector<unsigned char> users_size;
    vector<unsigned char> users;

    void setType(int type, int flag);

    void setMyNonce(int nonce_a, int flag);

    void setFriendNonce(int nonce_b, int flag);

    int setResponse(unsigned char *response, size_t dim, int flag);

    int setAddress(unsigned char *address, size_t dim, int flag);

    void setPort(int port, int flag);

    int setUserName(unsigned char *username, size_t dim, int flag);

    void setGameMove(int game_move);

    int setKey(unsigned char *key, size_t dim, int flag);

    void setUsers(vector<unsigned char> users);

    int setPeyload(vector<unsigned char> payload);

    vector<unsigned char> getPayload();

    vector<unsigned char *> getUsersList();

private:
    void setUsersSize(int users_size);

    size_t getEndVector(vector<unsigned char> v);
};

#endif //CLIENTPROJECTSECURITY_PAYLOAD_H
