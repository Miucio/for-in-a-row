//
// Created by Federico Cosimo Lapenna on 02/05/2020.
//

#include <iostream>
#include <cstdio>
#include "ForInARow.h"
#include "ClientOp.h"
#include "../common/operations/Operations.h"

#define ROW 6
#define COLUMN 7

int field[ROW][COLUMN], color[ROW][COLUMN];
int column;
int i, k, result;
bool finish = false, full;

using namespace std;

void view() {
    for (i = 0; i < ROW; i++) {
        for (k = 0; k < COLUMN; k++) {
            printf("%c%c%c%c%c", '_', '_', '_', '_', '_');
        }
        cout << endl;
        for (k = 0; k < COLUMN; k++) {
            switch (field[i][k]) {

                case 0:
                    printf("%c   %c", '|', '|');
                    break;
                case 1:
                    printf("%c x %c", '|', '|');
                    break;
                case 2:
                    printf("%c 0 %c", '|', '|');
                    break;
            }
        }
        cout << endl;
        for (k = 0; k < COLUMN; k++) {
            printf("%c%c%c%c%c", '_', '_', '_', '_', '_');
        }
        cout << endl;
    }
    cout << endl;
}

int userChoice(Session *pSession, Client *pClient) {
    cout << "\nChoose the column(Pay attention first column is 0 not 1) : ";
    while (!(cin >> column) || column < 0 || column > ROW - 1) {
        // Explain Error
        cout << "\nERROR: You must enter a valid number, range is 0-5";
        // Clear input stream
        cin.clear();
        // Discard previous input
        cin.ignore(132, '\n');
        column = 0;
        cout << "\nChoose the column(Pay attention first column is 0 not 1) : ";
    }
    full = true;
    i = ROW - 1;
    while (full == true) {
        if (i < 0) {
            cout << "column full!!!";
            userChoice(pSession, pClient);
        }
        if (field[i][column] == 0) {
            field[i][column] = 1;
            full = false;
            if (SendGameChose(column, pSession, pClient) < 0) {
                cerr << "error to send your chose" << '\n';
                return -1;
            }

        } else i--;
    }

    return 1;
}

int AdversaryChoise(Session *pSession, Client *pClient) {
    int adverasty_chose = ReceiveGameChoise(pSession);
    if (adverasty_chose < 0) {
        cerr << "column less than 0 " << '\n';
        goto err;
    }

    full = true;
    i = ROW - 1;
    while (full == true) {
        if (i < 0) {
            cerr << "column full!!!";
            goto err;
        }
        if (field[i][adverasty_chose] == 0) {
            field[i][adverasty_chose] = 2;
            full = false;
        } else i--;
    }
    if (SendOk(pSession, pClient->pkey, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error when try to send ok after autentication" << "\n";
        goto err;
    }

    return SUCCEED;

    err:
    char *err = "Error when try to set column";
    cerr << err << "\n";
    if (SendError(pSession, pClient->pkey, err, GCM_MESSAGE, NO_CHECK_SIGN) < 0) {
        cerr << "Error to send error message to server " << "\n";
        return FAIL;
    }
    return FAIL;
}

void control() {
    for (i = 0; i < ROW; i++) {
        for (k = 0; k < COLUMN; k++) {
            if (field[i][k] != 0) {
                if (field[i][k] == field[i - 1][k - 1] && field[i][k] == field[i - 2][k - 2] &&
                    field[i][k] == field[i - 3][k - 3]) {
                    color[i][k] = 12;
                    color[i - 1][k - 1] = 12;
                    color[i - 2][k - 2] = 12;
                    color[i - 3][k - 3] = 12;
                    finish = true;
                    result = field[i][k];

                }
                if (field[i][k] == field[i - 1][k] && field[i][k] == field[i - 2][k] &&
                    field[i][k] == field[i - 3][k]) {
                    color[i][k] = 12;
                    color[i - 1][k] = 12;
                    color[i - 2][k] = 12;
                    color[i - 3][k] = 12;
                    finish = true;
                    result = field[i][k];
                }
                if (field[i][k] == field[i - 1][k + 1] && field[i][k] == field[i - 2][k + 2] &&
                    field[i][k] == field[i - 3][k + 3]) {
                    color[i][k] = 12;
                    color[i - 1][k + 1] = 12;
                    color[i - 2][k + 2] = 12;
                    color[i - 3][k + 3] = 12;
                    finish = true;
                    result = field[i][k];
                }
                if (field[i][k] == field[i][k - 1] && field[i][k] == field[i][k - 2] &&
                    field[i][k] == field[i][k - 3]) {
                    color[i][k] = 12;
                    color[i][k - 1] = 12;
                    color[i][k - 2] = 12;
                    color[i][k - 3] = 12;
                    finish = true;
                    result = field[i][k];
                }
                if (field[i][k] == field[i][k + 1] && field[i][k] == field[i][k + 2] &&
                    field[i][k + 3] == field[i][k]) {
                    color[i][k] = 12;
                    color[i][k + 1] = 12;
                    color[i][k + 2] = 12;
                    color[i][k + 3] = 12;
                    finish = true;
                    result = field[i][k];
                }
                if (field[i][k] == field[i + 1][k - 1] && field[i][k] == field[i + 2][k - 2] &&
                    field[i][k] == field[i + 3][k - 3]) {
                    color[i][k] = 12;
                    color[i + 1][k - 1] = 12;
                    color[i + 2][k - 2] = 12;
                    color[i + 3][k - 3] = 12;
                    finish = true;
                    result = field[i][k];
                }
                if (field[i][k] == field[i + 1][k] && field[i][k] == field[i + 2][k] &&
                    field[i][k] == field[i + 3][k]) {
                    color[i][k] = 12;
                    color[i + 1][k] = 12;
                    color[i + 2][k] = 12;
                    color[i + 3][k] = 12;
                    finish = true;
                    result = field[i][k];
                }
                if (field[i][k] == field[i + 1][k + 1] && field[i][k] == field[i + 2][k + 2] &&
                    field[i][k] == field[i + 3][k + 3]) {
                    color[i][k] = 12;
                    color[i + 1][k + 1] = 12;
                    color[i + 2][k + 2] = 12;
                    color[i + 3][k + 3] = 12;
                    finish = true;
                    result = field[i][k];
                }
            }
        }
    }
}

int RunGame(Session *pSession, Client *pClient, bool first_turn) {
    for (i = 0; i < ROW; i++) {
        for (k = 0; k < COLUMN; k++) {
            field[i][k] = 0;
            color[i][k] = 15;
        }
    }
    finish = false;
    cout << endl << endl;
    cout << "||======================================================================||\n";
    cout << "||======================================================================||\n";
    cout << "||          __________   ____ _____   ___      __ __                    ||\n";
    cout << "||         / ____/ __ \\/ __ /__  /   /   |    / // /                    ||\n";
    cout << "||        / /_  / / / / /_/ /  / /  / /| |   / // /_                    ||\n";
    cout << "||       / __/ / /_/ / _, _/  / /__/ ___ |  /__  __/                    ||\n";
    cout << "||      /_/    \\____/_/ |_| /____ /_/  |_|    /_/                       ||\n";
    cout << "||======================================================================||\n";
    cout << "||======================================================================||\n";

    while (1) {
        if (first_turn) {
            if (userChoice(pSession, pClient) < 0) {
                cout << "return on menú" << '\n';
                return FAIL;
            }
            view();
            control();
        }
        first_turn = true;
        cout << "wait adversary..." << '\n';
        if (finish == true) {
            break;
        }
        if (AdversaryChoise(pSession, pClient) < 0) {
            cout << "return on menú" << '\n';
            return FAIL;
        }
        view();
        control();
        if (finish == true) {
            break;
        }
    }
    view();
    cout << endl;
    if (result == 1) {
        cout << "YOU WON";
    } else
        cout << "YOU LOSE!";
    cout << endl << endl;

    return SUCCEED;
}
