//
// Created by Federico Cosimo Lapenna on 09/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_CLIENTOP_H
#define CLIENTPROJECTSECURITY_CLIENTOP_H

#include "model/Client.h"
#include "../common/model/Session.h"

int Authenticate(Session *pSession, Client *pClient);

int OpenSession(const char *pHostname, int port);

int SendClientHello(Client *pClient, Session *pSession);

void Application(Client *pClient, Session *pSession);

int ReceiveGameChoise(Session *pSession);

int SendGameChose(int column, Session *pSession, Client *pClient);

std::vector<unsigned char *> GetListByMessage(std::vector<unsigned char> v);

#endif //CLIENTPROJECTSECURITY_CLIENTOP_H
