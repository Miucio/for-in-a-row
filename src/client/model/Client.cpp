//
// Created by Federico Cosimo Lapenna on 28/05/2020.
//
#include <openssl/x509.h>
#include <iostream>
#include <openssl/pem.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <zconf.h>
#include "Client.h"
#include "../../common/typeMessage.h"

Client::Client() {}

Client::Client(const Client &obj) {
    if (obj.pkey) {
        pkey = obj.pkey;
    }
    if (obj.store) {
        store = obj.store;
    }
}

int Client::Init(const char *key) {

    static char ok_chars[] = "qwertyuiopasdfghjklzxcvbnm"
                             "QWERTYUIOPASDFGHJKLZXCVBNM"
                             "1234567890_-.@";

    // on code to semplify the code runing
    const char *cA = "Federico_Lapenna_cert.pem";
    const char *crl = "Federico_Lapenna_crl.pem";
    //char *user = new char[USER_NAME_SIZE_MAX];
    //char *psw = new char[200];
    std::string user;
    std::string psw;

    do { //add username for the login
        std::cout << "insert username(max 20 car.): ";
        std::getline(std::cin, user);
        if (!std::cin) {
            std::cerr << "std in error" << '\0';
            return -1;
        }
    } while (user.empty() || user.size() > 20 ||
             strspn(user.c_str(), ok_chars) < user.size());

    do { //add password for the login
        std::cout << "insert password(max 100 car.): ";
        std::getline(std::cin, psw);
        if (!std::cin) {
            std::cerr << "std in error" << '\0';
            return -1;
        }
    } while (psw.empty() || psw.size() > 20 ||
             strspn(psw.c_str(), ok_chars) < psw.size());

    user_name_len = user.size();
    user_name = new unsigned char[user_name_len];
    memcpy(user_name, user.c_str(), user_name_len);

    LoadKeyCaAndcrl(key, crl, cA, psw.data());
    return 1;
}

void Client::LoadKeyCaAndcrl(const char *pKeyFile, const char *pCRLFIle,
                             const char *pCa, char *psw) {
    X509_CRL *pCrl;
    EVP_PKEY *pKey;
    X509_STORE *pStore = X509_STORE_new();
    FILE *pFile;

    pFile = fopen(pCRLFIle, "r");
    if (!pFile) {
        std::cerr << "Error:impossible open the crl file " << "\n";
        abort();
    }

    pCrl = PEM_read_X509_CRL(pFile, NULL, NULL, NULL);
    if (!pCrl) {
        std::cerr << "Error:impossible load the crl " << "\n";
        abort();
    }
    if (X509_STORE_add_crl(pStore, pCrl) < 0) {
        std::cerr << "ERROR to add cert on store" << "\n";
        abort();
    }
    fclose(pFile);

    pFile = fopen(pKeyFile, "r");
    if (!pFile) {
        std::cerr << "Error:impossible open the key file " << "\n";
        abort();
    }

    pKey = PEM_read_PrivateKey(pFile, NULL, NULL, psw);
    if (!pKey) {
        std::cerr << "Error:impossible load the private key " << "\n";
        abort();
    }

    Client::pkey = pKey;
    fclose(pFile);

    X509 *pCa_cert;

    pFile = fopen(pCa, "r");
    if (!pFile) {
        std::cerr << "ERROR to open file" << "\n";
        abort();
    }
    pCa_cert = PEM_read_X509(pFile, NULL, NULL, NULL);
    if (!pCa_cert) {
        std::cerr << "ERROR to read cert" << "\n";
        abort();
    }
    if (X509_STORE_add_cert(pStore, pCa_cert) < 0) {
        std::cerr << "ERROR to add cert on store" << "\n";
        abort();
    }
    X509_STORE_set_flags(pStore, X509_V_FLAG_CRL_CHECK); //CHECK SRL

    Client::store = pStore;
    fclose(pFile);
}

int Client::OpenListener(int port) {
    master_socket = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    int i = 0;

    //try 5 times
    while (::bind(master_socket, (struct sockaddr *) &addr, sizeof(addr)) != 0) {
        if (i == 5) {
            std::cerr << "impossible bind port, close" << "\n";
            return FAIL;
        }
        std::cerr << "can't bind port" << "\n" << "try again " << '\n';
        sleep(5);
        i++;
    }
    if (listen(master_socket, 1) != 0) {
        std::cerr << "Can't configure listening port" << "\n";
        return FAIL;
    }
    return master_socket;
}

void Client::CloseListener() {
    close(master_socket);
}






