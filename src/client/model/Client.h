//
// Created by Federico Cosimo Lapenna on 28/05/2020.
//

#ifndef CLIENTPROJECTSECURITY_CLIENT_H
#define CLIENTPROJECTSECURITY_CLIENT_H

#include <openssl/ossl_typ.h>
#include <netinet/in.h>

class Client {
public:
    X509_STORE *store;
    EVP_PKEY *pkey;
    unsigned char *user_name;
    size_t user_name_len = 0;
    struct sockaddr_in addr;
    int master_socket;

    Client();

    Client(const Client &obj);

    int Init(const char *key);

    int OpenListener(int port);

    void CloseListener();

private:

    void LoadKeyCaAndcrl(const char *pKeyFile, const char *pCRLFIle, const char *pCa, char *psw);

};

#endif //CLIENTPROJECTSECURITY_CLIENT_H
