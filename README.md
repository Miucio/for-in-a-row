# For In A Row 
Project for Security course when two client play to for in a row with a secure connection.


##Client Password

```bash
Alice --> aliceunipi
Bob   --> bobunipi
```
##Run
Insert in the same folder of executable all the certificate and key files.You can find everything in the resources folder.

Server doesn't need parameters,instead you should run client like this:

```bash
./client Alice_cert.pem Alice_key_psw.pem
```

##How to use?

